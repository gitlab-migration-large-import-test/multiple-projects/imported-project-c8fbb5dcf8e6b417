# frozen_string_literal: true

module V2
  class Users < Grape::API
    include AuthenticationSetup
    include Pagination

    helpers do
      include ApplicationHelper

      def find_user(username: params[:username])
        User.find_by(username: username)
      end
    end

    namespace :users do
      desc "Get all users" do
        detail "Return array of all users"
        success model: User::Entity, examples: [User::Entity.example_response], message: "Successful response"
        is_array true
      end
      params do
        use :pagination_params
      end
      get do
        present paginate(User.all)
      end

      desc "Add user" do
        detail "Add new user"
        success model: User::Entity, examples: User::Entity.example_response, message: "Successful response"
        failure [{ code: 409, message: "Project already exists" }]
      end
      params do
        requires :username, type: String, desc: "User name"
        requires :password, type: String, desc: "User password"
      end
      post do
        username = params[:username]
        user = find_user(username: username)
        error!("User already exists", 409) if user
      rescue Mongoid::Errors::DocumentNotFound
        log(:info, "Creating user '#{username}'")
        present User.create!(username: username, password: params[:password])
      end

      route_param :username, type: String, desc: "User name" do
        desc "Get single user" do
          detail "Return single user"
          success model: User::Entity, examples: User::Entity.example_response, message: "Successful response"
          failure [{ code: 404, message: "User not found" }]
        end
        get do
          present find_user
        end

        desc "Update user password" do
          detail "Update user password"
          success model: User::Entity, examples: User::Entity.example_response, message: "Successful response"
          failure [{ code: 404, message: "User not found" }]
        end
        params do
          requires :password, type: String, desc: "User password"
        end
        put do
          present find_user.update_attributes!(password: params[:password])
        end

        desc "Remove user" do
          detail "Remove user from database"
          success message: "Removed user"
          failure [{ code: 404, message: "User not found" }]
        end
        delete do
          find_user.delete
        end
      end
    end
  end
end
