# frozen_string_literal: true

describe Mr::RecreateJob, type: :job do
  subject(:job) { described_class }

  let(:project_name) { "project" }
  let(:mr_iid) { 1 }
  let(:discussion_id) { 123 }

  before do
    allow(Update::Routers::MergeRequest::Recreate).to receive(:call)
  end

  it { is_expected.to be_retryable false }

  it "performs enqued job" do
    job.perform_now(project_name, mr_iid, discussion_id)

    expect(Update::Routers::MergeRequest::Recreate).to have_received(:call).with(
      project_name: project_name,
      mr_iid: mr_iid,
      discussion_id: discussion_id
    )
  end
end
