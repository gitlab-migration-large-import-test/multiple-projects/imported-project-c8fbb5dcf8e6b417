import { Locator, Page, expect } from "@playwright/test";

export class NewProjectPage {
  readonly page: Page;
  readonly projectInput: Locator;
  readonly tokenInput: Locator;
  readonly addButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.projectInput = page.locator("id=project_name");
    this.tokenInput = page.locator("id=access_token");
    this.addButton = page.locator("id=add_project");
  }

  async visit() {
    await this.page.goto("/new_project");
  }

  async expectPageToBeVisible() {
    await expect(this.projectInput).toBeVisible();
    await expect(this.tokenInput).toBeVisible();
    await expect(this.addButton).toBeVisible();
  }

  async addProject(projectName: string, token?: string) {
    await this.projectInput.fill(projectName);
    if (token) {
      await this.tokenInput.fill(token);
    }
    await this.addButton.click();
  }
}
