# frozen_string_literal: true

FactoryBot.define do
  factory :update_failure, class: "Update::Failure" do
    message { "Error message" }
    backtrace { "Full error backtrace" }

    run { create(:update_run) }
  end
end
