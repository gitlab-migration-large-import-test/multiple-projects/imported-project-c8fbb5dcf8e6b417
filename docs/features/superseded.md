# Closing outdated merge requests

When a newer version update merge request is created for specific dependency, application will automatically close previous update merge requests. No additional configuration is required.
