# frozen_string_literal: true

class RedisConnectionPool
  # rubocop:disable Naming/MemoizedInstanceVariableName
  def self.fetch
    @redis_pool ||= RedisClient.config(**AppConfig.redis_config).new_pool(size: ENV.fetch("RAILS_MAX_THREADS", 5))
  end
  # rubocop:enable Naming/MemoizedInstanceVariableName
end

Healthcheck.configure do |config|
  config.success = 200
  config.error = 503
  config.verbose = false
  config.route = "/healthcheck"
  config.method = :get

  # -- Checks --
  config.add_check :database, lambda {
    begin
      Mongoid.default_client.database_names.present?
    rescue StandardError => e
      ApplicationHelper.log(:error, "MongoDB healthcheck failed - #{e.message}", tags: ["Healthcheck"])
      raise e
    end
  }
  config.add_check :redis, lambda {
    begin
      RedisConnectionPool.fetch.with do |redis|
        redis.call("PING")
        redis.close
      end
    rescue StandardError => e
      ApplicationHelper.log(:error, "Redis healthcheck failed - #{e.message}", tags: ["Healthcheck"])
      raise e
    end
  }
end
